#include "TitleScene.h"
#include "PlayScene.h"
#include "../Engine/Image.h"

const int HALF_SECOND = 30;		//0.5秒(30f)

TitleScene::TitleScene(IGameObject * parent)
{
}

TitleScene::~TitleScene()
{
}

void TitleScene::Initialize()
{
	titlePict_ = Image::Load( "data/titleScene.png" );
	assert( titlePict_ >= 0 );

	controlText_ = Image::Load("data/PRESS_ENTER_START_TEXT.png");
	assert(controlMat_ >= 0);

	controlTextNoBorder_ = Image::Load("data/PRESS_ENTER_START_TEXT(BORDERLESS).png");
	assert(controlTextNoBorder_ >= 0);

	D3DXMatrixTranslation(&controlMat_, 150, 500, 0);

}

void TitleScene::Update()
{
	//Enterキーで遷移
	if ( Input::IsKeyDown( DIK_RETURN) )
	{
		//プレイシーンに移行
		SceneManager::ChangeScene( SCENE_ID_PLAY );
	}
}

void TitleScene::Draw()
{
 	Image::SetMatrix(titlePict_, worldMatrix_);
	Image::Draw(titlePict_);

	//「PRESS ENTER START」のテキストを0.5秒ごとに入れ替え表示する
	static int time = 0;
	static int cnt = 0;

	if ( cnt % 2 == 0 ){
		Image::SetMatrix(controlText_, worldMatrix_ * controlMat_);
		Image::Draw(controlText_);
	}

	else
	{
		Image::SetMatrix(controlTextNoBorder_, worldMatrix_ * controlMat_);
		Image::Draw(controlTextNoBorder_);
	}

	time++;
	//0.5秒経ったら画像を入れ替え
	if( time > HALF_SECOND )
	{
		cnt++;
		time = 0;
	}
}

void TitleScene::Release()
{
}
