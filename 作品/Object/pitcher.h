#pragma once
#include "../Engine/Global.h"
#include "Ball.h"

class Pitcher : public IGameObject
{
	Ball *pBall;				//ボールのオブジェクト
	D3DXVECTOR3 playerRatate_;	//バットの回転情報
	int ball_cnt_;				//ボールの投げた回数

	bool IsBoolAppear_;			//ボールが出されているか

public:
	Pitcher(IGameObject* parent);
	~Pitcher();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ボールにバットの回転情報を送る
	void RevRotate(D3DXVECTOR3 rotate) { playerRatate_ = rotate; };

	//ボールから生死状態をもらう
	void SetIsBoolAppear(bool IsDead) { IsBoolAppear_ = IsDead; };
	
	//ボールの投げた回数を渡す
	int GetBallCnt() { return ball_cnt_; };
};

