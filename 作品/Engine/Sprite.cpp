#include "Sprite.h"
#include "Direct3D.h"

Sprite::Sprite() :
		pSprite_(nullptr),
		pTexture_(nullptr)
{
}

Sprite::~Sprite()
{
	SAFE_RELEASE( pTexture_ );
	SAFE_RELEASE( pSprite_ );
}

//画像ファイルをロード
//第一引数 ... string型 ファイルの名前
//戻り値   ... HRESULT型 ハンドル番号
HRESULT Sprite::Load(std::string fileName)
{
	//スプライトオブジェクトの作成
	D3DXCreateSprite(Direct3D::pDevice, &pSprite_ );

	//テクスチャオブジェクトの作成
	HRESULT hr;
	hr = D3DXCreateTextureFromFileEx(Direct3D::pDevice, fileName.c_str(), 0, 0, 0, 0,
		D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &pTexture_	);

	return hr;
}

//引数の型に&をつけることによって参照渡しとなり、4バイト分のアドレスを送ることだけで同じことができる
void Sprite::Draw(const D3DXMATRIX& matrix, RECT* rect)
{
	pSprite_->SetTransform(&matrix);
	
	//ゲーム画面の描画
	pSprite_->Begin(D3DXSPRITE_ALPHABLEND);
	pSprite_->Draw(pTexture_, rect, nullptr, nullptr, D3DCOLOR_ARGB( 255, 255, 255, 255 ));
	pSprite_->End();
}

//テクスチャのサイズを取得
D3DXVECTOR2 Sprite::GetTextureSize()
{
	D3DXVECTOR2 size;
	D3DSURFACE_DESC d3dds;

	pTexture_->GetLevelDesc( 0, &d3dds );
	size.x = (float)d3dds.Width;
	size.y = (float)d3dds.Height;

	return size;
}