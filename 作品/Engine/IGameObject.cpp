#include "IGameObject.h"
#include "Global.h"
#include "Model.h"
#include "../Engine/Collider/Collider.h"

//RootJob* IGameObject::pRootjob_ = new RootJob;

//引数が未入力の場合のコンストラクタ
IGameObject::IGameObject() :
	IGameObject(nullptr, "")	//nullptr , 無名としてコンストラクタに送る
{
}

//引数が親のみの場合のコンストラクタ
IGameObject::IGameObject(IGameObject * parent) :
	IGameObject(parent, "")		//parent , 無名としてコンストラクタに送る
{
}

//基本的にはこのコンストラクタが呼ばれる
IGameObject::IGameObject(IGameObject * parent, const std::string & name) :
	//初期化
	pParent_(parent),
	name_(name),
	position_(D3DXVECTOR3(0, 0, 0)),
	rotate_(D3DXVECTOR3(0, 0, 0)),
	scale_(D3DXVECTOR3(1, 1, 1)),
	dead_(false),
	pCollider_(nullptr)
{
	state_ = { 0, 1, 1, 0 };
}

IGameObject::~IGameObject()
{
	//開放する際、親をデリートしたら子も死ぬのでここでデリートしちゃだめ
	//SAFE_DELETE(pCollider_);

	for (auto it = colliderList_.begin(); it != colliderList_.end(); it++)
	{
		SAFE_DELETE(*it);
	}
	colliderList_.clear();
}


//行列を作成するメソッド
void IGameObject::Transform()
{
	//行列を作成
	D3DXMATRIX matp;
	D3DXMATRIX matrx;
	D3DXMATRIX matry;
	D3DXMATRIX matrz;
	D3DXMATRIX mats;

	//単位行列を作成
	//D3DXMatrixIdentity(&mat);	

	//移動行列を作成
	D3DXMatrixTranslation(&matp, position_.x, position_.y, position_.z);

	//回転行列を作成
	D3DXMatrixRotationX(&matrx, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&matry, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&matrz, D3DXToRadian(rotate_.z));

	//拡大縮小行列を作成
	D3DXMatrixScaling(&mats, scale_.x, scale_.y, scale_.z);

	//行列を組み合わせる
	localMatrix_ =  mats *  matrz * matrx * matry * matp;

	//ワールドマトリックスを計算。
	//親のワールドマトリックスにローカルマトリックスになっている
	//できない...。worldMatrixの中身が不定だからなのでは？
	if(GetParent() == nullptr)
	{
		worldMatrix_ = localMatrix_;
	}

	else
	{
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_;
	}
}


void IGameObject::ReleaseSub()
{
	////とりあえず自分のReleaseを呼ぶ
	//Release();

	////そのあと、子のReleaseを呼ぶ
	//for (auto it = pChildlist_.begin(); it != pChildlist_.end(); it++)
	//{
	//	(*it)->ReleaseSub();
	//	SAFE_DELETE(*it);
	//}
}

//自滅する関数
void IGameObject::KillMe()
{
	//これから消えるフラグを立てる
	state_.dead = 1;
}

//親情報を受け取るゲッター
IGameObject * IGameObject::GetParent()
{
	return pParent_;
}

//positionを受け取るゲッター
D3DXVECTOR3 IGameObject::Getposition()
{
	return position_;
}

//回転情報を受け取るゲッター
D3DXVECTOR3 IGameObject::Getrotate()
{
	return rotate_;
}

//大きさを受け取るゲッター
D3DXVECTOR3 IGameObject::Getscale()
{
	return scale_;
}

void IGameObject::PushBackChild(IGameObject* pObj)
{
	pChildlist_.push_back(pObj);
}

IGameObject * IGameObject::FindChildObject(const std::string & name)
{
	//子供がいないなら終わり
	if (pChildlist_.empty())
		return nullptr;

	//イテレータ
	auto it = pChildlist_.begin();	//先頭
	auto end = pChildlist_.end();	//末尾

	//子オブジェクトから探す
	while (it != end) {
		//同じ名前のオブジェクトを見つけたらそれを返す
		if ((*it)->GetObjectName() == name)
			return *it;

		//その子供（孫）以降にいないか探す
		IGameObject* obj = (*it)->FindChildObject(name);
		if (obj != nullptr)
		{
			return obj;
		}

		//次の子へ
		it++;
	}

	//見つからなかった
	return nullptr;
}

//オブジェクトの名前を取得
const std::string& IGameObject::GetObjectName(void) const
{
	return objectName_;
}

//衝突判定
//引数 : pTarget 衝突した相手
void IGameObject::Collision(IGameObject* pTarget)
{
	int a = 0;
	//自分同士の当たり判定はしない
	if ( this == pTarget )
	{
		return;
	}
	//ここまでの挙動は同じだとみられる

	//ここPlayerしか呼ばれてないんだが
	//自分とpTargetのコリジョン情報を使って当たり判定
	//1つのオブジェクトが複数のコリジョン情報を持ってる場合もあるので二重ループ
	for (auto i = this->colliderList_.begin(); i != this->colliderList_.end(); i++)
	{
		int a = 0;
		for ( auto j = pTarget->colliderList_.begin(); j != pTarget->colliderList_.end(); j++ )
		{
			int a = 0;
			if ((*i)->IsHit(*j))
			{
				//当たった
				int a = 0;
				this->OnCollision( pTarget );
			}
		}
	}

	//子供がいないのなら終了
	if (pTarget->pChildlist_.empty())
		return;

	for ( auto i = pTarget->pChildlist_.begin(); i != pTarget->pChildlist_.end(); i ++ )
	{
		int a = 0;
		Collision(*i);
	}
}

void IGameObject::AddCollider(Collider* collider)
{
	collider->SetGameObjct(this);
	colliderList_.push_back(collider);
}

