#pragma once
#include <vector>
#include <string>
#include "Fbx.h"

//ここではクラスではなくnamespaceになっている
namespace Model
{
	//モデル情報を入れる構造体
	struct ModelData
	{
		std::string fileName;	//ファイル名
		Fbx* pFbx;				//ロードしたモデルデータのアドレス
		D3DXMATRIX matrix;		//行列

		//コンストラクタで初期化を行う
		ModelData() : fileName(""), pFbx(nullptr)
		{
			//単位行列にすることで初期化
			D3DXMatrixIdentity(&matrix);
		}
	};
	int Load(std::string fileName);					//ロードしてモデル番号を受け取る
	void Draw(int handle);							//それをもとに描画する
	void SetMatrix(int handle, D3DXMATRIX& matrix);	//セッター
	void Release(int handle);
	void RayCast(int handle, RayCastData & data);
	//開放処理
	void AllRelease();

};

