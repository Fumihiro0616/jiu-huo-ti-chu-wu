#pragma once
#include "../Global.h"

//class IGameObject;

enum ColliderType
{
	COLLIDER_BOX,		//箱型
	COLLIDER_CIRCLE,	//球型
	COLLIDER_CYLINDER	//円柱
};

class Collider
{
	friend class BoxCollider;
	friend class SphereCollider;
	friend class CylinderCollider;

protected:

	IGameObject* pGameObject_;
	D3DXVECTOR3 size_;		//モデルの大きさ
	D3DXVECTOR3 center_;	//モデルの原点
	float		radius_;	//モデルの半径
	IGameObject* owner_;	//誰のコライダーなのか
	ColliderType type_;		//コライダーの種類
	LPD3DXMESH	pMesh_;		//テスト表示用の枠
	D3DXMATRIX retMat_;

public:
	Collider();
	~Collider();

	bool IsHitCyliVsSqhere(BoxCollider * line, SphereCollider * p);

	//当たっているか否か
	virtual bool IsHit(Collider* target) = 0;

	void SetGameObjct(IGameObject* gameObject) { pGameObject_ = gameObject; };

	//テスト表示用の枠を描画
	//引数：position	位置
	void Draw(D3DXVECTOR3 position);
	bool Collision_OBBToSphere(BoxCollider* obb, SphereCollider* p);
	bool IsShapeAngle(CylinderCollider* obb, SphereCollider * p, int side);
	//bool IsShapeAngle(D3DXVECTOR3 pointA, D3DXVECTOR3 pointB, D3DXVECTOR3 pointC);
	float PointToLineDist(SphereCollider* p, CylinderCollider* cylinder, D3DXVECTOR3 &h, float &t);
	bool Collision_CylinderToSphere( SphereCollider* p, CylinderCollider* cylinder );
	float Calc_Projection_Angle_(SphereCollider * p, CylinderCollider * cylinder);
	void TransformCollider();
};