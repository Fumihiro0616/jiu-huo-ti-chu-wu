#pragma once
#include "Collider.h"

class SphereCollider : public Collider
{
	friend class Collider;

	float		radius_;
	D3DXVECTOR3 size_;

public:
	SphereCollider( D3DXVECTOR3 center, float radius );
	~SphereCollider();

private:
	bool IsHit( Collider* target ) override;
};

