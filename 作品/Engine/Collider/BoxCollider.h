#pragma once
#include "../Collider/Collider.h"

class BoxCollider : public Collider
{
	friend class Collider;

	D3DXVECTOR3 size_;		//�T�C�X
	D3DXVECTOR3 rotate_;	//��]

public:
	BoxCollider(D3DXVECTOR3 basePos, D3DXVECTOR3 size );
	~BoxCollider();

private:
	bool IsHit(Collider* target) override;
	D3DXVECTOR3 GetDirect( int dir );
	float GetSize( int dir );
	D3DXVECTOR3 GetStatingPoint();
	D3DXVECTOR3 GetEndPoint();
};

