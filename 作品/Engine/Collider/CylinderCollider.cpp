#include "CylinderCollider.h"

CylinderCollider::CylinderCollider(D3DXVECTOR3 center, D3DXVECTOR3 posE, float radius)
{
	//円柱の情報を定義
	center_ = center;

	posE_ = posE;

	radius_ = radius;
	dir_ = GetDirVec();

	origin_ = D3DXVECTOR3( center_.x, center_.y, -2.0f );

	type_ = COLLIDER_CYLINDER;
}

CylinderCollider::~CylinderCollider()
{
}

//方向ベクトルを算出( バットが回っている時の回転情報を出すときに使う )
D3DXVECTOR3 CylinderCollider::GetDirVec()
{
	D3DXVec3Normalize(&dir_, &(center_ - posE_));
	return dir_;
}

bool CylinderCollider::IsHit(Collider * target)
{
	//当たり判定
	if (target->type_ == COLLIDER_CIRCLE)
	{
		return Collision_CylinderToSphere((SphereCollider*)target, this);
	}
	return false;
}