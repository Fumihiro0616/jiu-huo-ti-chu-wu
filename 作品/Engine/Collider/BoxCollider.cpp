#include "BoxCollider.h"
//#include "SphereCollider.h"

BoxCollider::BoxCollider(D3DXVECTOR3 basePos, D3DXVECTOR3 size)
{
	//こいつに始点と終点を出す機能も入れるか
	//centerとsizeでどっちサイズに伸びているかわかるハズ
	center_ = basePos;		//中央の座標
	size_ = size;
	type_ = COLLIDER_BOX;
}

BoxCollider::~BoxCollider()
{
	
}

bool BoxCollider::IsHit(Collider* target)
{
	//if (target->type_ == COLLIDER_BOX)
	//	//return IsHitBoxVsBox((BoxCollider*)target, this);
	//	return false;

	//else 
	if (target->type_ == COLLIDER_CIRCLE)
	{
		IsHitCyliVsSqhere(this, (SphereCollider*)target);
		return Collision_OBBToSphere( this, (SphereCollider*)target);
	}

	return false;
}

D3DXVECTOR3 BoxCollider::GetDirect(int dir)
{
	switch ( dir )
	{
		case 0: return D3DXVECTOR3(1, 0, 0);
		case 1: return D3DXVECTOR3(0, 1, 0);
		case 2: return D3DXVECTOR3(0, 0, 1);
	}
	return D3DXVECTOR3();
}

float BoxCollider::GetSize(int dir)
{
	switch (dir)
	{
	case 0: return size_.x;
	case 1: return size_.y;
	case 2: return size_.z;
	}

	return 0.0f;
}

//勝手に始点のほうが右、終点を左のほうにある点とする(仮)
//これを回転情報で変形させて使う

//何をもって始点とかなんも考えてない...
//始点を求める(横伸び前提。のちに円柱用のコライダーを作成)
D3DXVECTOR3 BoxCollider::GetStatingPoint()
{
	float x = center_.x + size_.x;
	float y = center_.y;
	float z = center_.z;
	return D3DXVECTOR3(x,y,z);
}

//終点を求める
D3DXVECTOR3 BoxCollider::GetEndPoint()
{
	float x = center_.x - size_.x;
	float y = center_.y;
	float z = center_.z;
	return D3DXVECTOR3(x, y, z);
}