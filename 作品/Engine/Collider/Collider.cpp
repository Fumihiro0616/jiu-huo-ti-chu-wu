#include "Collider.h"
#include "../Direct3D.h"
#include "BoxCollider.h"
#include "SphereCollider.h"
#include "CylinderCollider.h"
#include "../IGameObject.h"
#include "../SendData.h"

const float OBTUSE_ANGLE = 1.5708f;	//90°

Collider::Collider() :
	pGameObject_(nullptr)
{
}

Collider::~Collider()
{
}

//線分と球体が当たったかどうかの判定を行う
//第一引数 ... 当たった線分型のコライダー
//第二引数 ... 当たった球体型のコライダー
//戻り値   ... 当たったかどうか
bool Collider::IsHitCyliVsSqhere(BoxCollider* line, SphereCollider* p)
{
	//球の先端を取る
	D3DXVECTOR3 top_S = D3DXVECTOR3(
		p->pGameObject_->Getposition().x,
		p->pGameObject_->Getposition().y, 
		p->pGameObject_->Getposition().z - p->radius_);

	D3DXVECTOR3 cen_S = p->pGameObject_->Getposition();

	D3DXVECTOR3 cen_B = line->pGameObject_->Getposition();

	//xの長さを入れる
	FLOAT L = line->GetSize(0);

	FLOAT v12 = L * 2;

	//v1を求める
	D3DXVECTOR3 v1 = D3DXVECTOR3( cen_S.x - cen_B.x, cen_S.y - cen_B.y, cen_S.z - cen_B.z);

	D3DXVECTOR3 vn = D3DXVECTOR3( cen_B.x , cen_B.y, cen_S.z );

	//角度(radian)を求める
	D3DXVECTOR3 normalv1, normalvn;
	D3DXVec3Normalize( &normalv1, &v1 );
	D3DXVec3Normalize( &normalvn, &vn );
	float dot = D3DXVec3Dot( &normalv1, &normalvn);
	float angle = acos(dot);

	float d = D3DXVec3Dot(&vn, &v1) / D3DXVec3Length( &vn );
	d = fabs(d);	//絶対値に変換

	FLOAT n = 1;

	//最終的に長さを求めるベクトル
	D3DXVECTOR3 Vec(0, 0, 0);

	//回転情報
	D3DXVECTOR3 rotateTrans = line->pGameObject_->Getrotate();

	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotateTrans.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotateTrans.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotateTrans.z));

	retMat_ = rotateZ * rotateY * rotateX;

	D3DXVECTOR3 direct = D3DXVECTOR3(1,0,0);
	D3DXVec3TransformCoord(&direct, &direct, &retMat_);
	
	//内積を求める
	float s = D3DXVec3Dot(&(line->pGameObject_->Getposition() - (line->pGameObject_->Getposition())), &direct) / L;
	
	//OBBと球の距離が球の半径より短いか
	if (D3DXVec3Length(&Vec) < p->radius_)
	{
		//この時に法線をボールに教えておきたい
		SendData::SetNormal(direct);
		return true;
	}
	return false;
}

//テスト表示用の枠を描画(現在は未使用)
//引数：position	位置
void Collider::Draw(D3DXVECTOR3 position)
{
}

//ボックスと球体が当たったかどうかを判定する
//第一引数 ... ボックス型のコライダー
//第二引数 ... 球体型のコライダー
bool Collider::Collision_OBBToSphere(BoxCollider* obb, SphereCollider* p)
{
	// 最終的に長さを求めるベクトル
	D3DXVECTOR3 Vec(0, 0, 0);
	
	//obbの回転情報をもっておく
	D3DXVECTOR3 rotateTrans = obb->pGameObject_->Getrotate();

	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotateTrans.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotateTrans.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotateTrans.z));

	retMat_ = rotateZ * rotateY * rotateX;

	// 各軸についてはみ出た部分のベクトルを算出
	for (int i = 0; i < 3; i++)
	{
		FLOAT L = obb->GetSize(i);
		D3DXVECTOR3 direct = obb->GetDirect(i);
		D3DXVec3TransformCoord(&direct, &direct, &retMat_);

		D3DXVECTOR3 dist = p->pGameObject_->Getposition() - obb->pGameObject_->Getposition();

		if (L <= 0) continue;  // L=0は計算できない
		float s = D3DXVec3Dot(&(p->pGameObject_->Getposition() - (obb->pGameObject_->Getposition() + obb->center_)), &direct) / L;

		// sの値から、はみ出した部分があればそのベクトルを加算
		s = fabs(s);
		if (s > 1)
			Vec += (1 - s) * L * direct;   // はみ出した部分のベクトル算出
	}

	//OBBと球の距離が球の半径より短いか
	if (D3DXVec3Length(&Vec) < p->radius_)
	{
		return true;
	}

	return false;
}

//点と線分は鋭角かどうか
//第一引数 ... 円柱型のコライダー
//第二引数 ... 球体型のコライダー
//第三引数 ... 球体は円柱からどっち側にいるか ( 1: 視点側, 2: 終点側 )
//戻り値   ... 鋭角だったかどうか
bool Collider::IsShapeAngle( CylinderCollider* cylinder, SphereCollider* p, int side )
{
	//直線の始点をワールド座標にする
	D3DXVECTOR3 posS = cylinder->pGameObject_->Getposition();

	//直線の終点をワールド座標にする
	D3DXVECTOR3 posE = cylinder->posE_ + cylinder->pGameObject_->Getposition();

	//直線の回転情報を取得
	D3DXVECTOR3 rotateTrans = cylinder->pGameObject_->Getrotate();

	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotateTrans.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotateTrans.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotateTrans.z));

	retMat_ = rotateZ * rotateY * rotateX;

	//直線の回転情報を反映
	D3DXVec3TransformCoord(&posE, &posE, &retMat_);
	D3DXVec3TransformCoord(&posS, &posS, &retMat_);

	//角度( ラジアン )
	float angle = 0.0f;

	//始点から見たボールとの角度
	if ( side == 0 )
	{
		//始点からの直線のベクトル
		D3DXVECTOR3 vl = posE - posS;

		//始点からの球のベクトル
		D3DXVECTOR3 vs = p->pGameObject_->Getposition() - posS;

		//正規化後、内積を求め角度を算出
		D3DXVec3Normalize(&vl, &vl );
		D3DXVec3Normalize( &vs, &vs );
		angle = D3DXVec3Dot( &vl, &vs );
		angle = acos( angle );
	}

	//終点から見たボールとの角度
	else if (side == 1)
	{
		//内積を計算( これは終点からみたパターン )
		D3DXVECTOR3 vl = posS - posE;
		D3DXVECTOR3 vs = p->pGameObject_->Getposition() - posE;

		D3DXVec3Normalize(&vl, &vl);
		D3DXVec3Normalize(&vs, &vs);
		angle = D3DXVec3Dot(&vl, &vs);
		angle = acos( angle );		
	}

	//鈍角だったらfalseを返す
	if ( OBTUSE_ANGLE < angle ) 
		return false;
	
	//鋭角だったらtrueを返す
	return true;
}

//点から直線の距離を求める
//第一引数 ... 球体型のコライダー
//第二引数 ... 円柱型のコライダー
//第三引数 ... 高さ
//戻り値   ... 距離
float Collider::PointToLineDist( SphereCollider* p, CylinderCollider* cylinder, D3DXVECTOR3 &h, float &t )
{
	//点と線分の距離を出す
	D3DXVECTOR3 posS = cylinder->pGameObject_->Getposition();
	posS.x += cylinder->center_.x;
	
	D3DXVECTOR3 posE = cylinder->posE_ + cylinder->pGameObject_->Getposition();

	D3DXVECTOR3 rotateTrans = cylinder->pGameObject_->Getrotate();

	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotateTrans.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotateTrans.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotateTrans.z));

	retMat_ = rotateZ * rotateY * rotateX;

	//バットの回転情報を考慮する
	D3DXVec3TransformCoord(&posE, &posE, &retMat_);
	D3DXVec3TransformCoord(&posS, &posS, &retMat_);

	D3DXVECTOR3 dir = posE - posS;

	//ベクトルの2乗を返す
	float lenSqV = D3DXVec3Dot(&dir, &dir);

	if (lenSqV > 0.0f)
	{
		t = D3DXVec3Dot((&dir),
			&((p->pGameObject_->Getposition()) - posS));
		t /= lenSqV;
	}

	h = cylinder->pGameObject_->Getposition() + t * dir;

	float len = D3DXVec3Length(&(h - p->pGameObject_->Getposition()));
	len -= 2.0f;
	//点と直線の内積の長さを返す
	return len;
}

//円柱と球の当たり判定
//第一引数 ... 球体型のコライダー
//第二引数 ... 円柱型のコライダー
//戻り値   ... 当たったかどうか
bool Collider::Collision_CylinderToSphere( SphereCollider* p, CylinderCollider* cylinder )
{

	//線分の終点を取得
	D3DXVECTOR3 posE = cylinder->posE_ + cylinder->pGameObject_->Getposition();
	D3DXVECTOR3 h = D3DXVECTOR3(0,0,0);
	float t = 0.0f;

	//点と線分の長さを求める
	float len = PointToLineDist( p, cylinder, h, t );

	//角度から端点の位置を探す
	//始点側
	if (IsShapeAngle(cylinder, p, 0) == false)
	{
		h = cylinder->pGameObject_->Getposition();
		len = D3DXVec3Length(&(cylinder->pGameObject_->Getposition() - p->pGameObject_->Getposition()));
	}

	//終点側
	else if (IsShapeAngle(cylinder, p, 1) == false)
	{
		h = posE;	
		len = D3DXVec3Length( &(posE - p->center_/*p->pGameObject_->Getposition()*/));
	}

	len += p->pGameObject_->Getposition().y - 1;

	//球の半径＋円柱の半径の値が幅より短かったら当たったことになる
	if (p->radius_ + cylinder->radius_ >= len)
	{
		Calc_Projection_Angle_( p, cylinder );
		return true;
	}

	return false;
}

//直線にボールが当たった時の角度
//第一引数 ... 球体型のコライダー
//第二引数 ... 円柱型のコライダー
//戻り値   ... 角度
float Collider::Calc_Projection_Angle_( SphereCollider* p, CylinderCollider* cylinder )
{
	D3DXVECTOR3 rotateTrans = cylinder->pGameObject_->Getrotate();

	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotateTrans.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotateTrans.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotateTrans.z));

	retMat_ = rotateZ * rotateY * rotateX;

	//直線の接触点
	D3DXVECTOR3 cyli_Hitpoint = cylinder->pGameObject_->Getposition();

	//球の接触点
	D3DXVECTOR3 sphere_Hitpoint = p->pGameObject_->Getposition();
	
	//直線の当たった位置のX軸を球のX軸に合わせる
	cyli_Hitpoint.x = sphere_Hitpoint.x;

	//直線から球の接触点へのベクトル
	D3DXVECTOR3 angle_Vec = sphere_Hitpoint - cyli_Hitpoint;

	D3DXVECTOR3 Z = D3DXVECTOR3(cyli_Hitpoint.x, cyli_Hitpoint.y, cyli_Hitpoint.z + 5.0f);
	D3DXVECTOR3 VecZ = Z - cyli_Hitpoint;

	D3DXVec3TransformCoord( &VecZ, &VecZ, &retMat_ );

	float angle;
	D3DXVec3Normalize(&angle_Vec, &angle_Vec);
	D3DXVec3Normalize(&VecZ, &VecZ);
	angle = D3DXVec3Dot(&VecZ, &angle_Vec);

	angle = acos(angle);

	if (angle_Vec.y < 0.0f)
		angle *= -1.0;

	SendData::SetCollisionAngle( angle );

	return angle;
}

void Collider::TransformCollider()
{
	
}
