#pragma once
#include "IGameObject.h"
#include "Direct3D.h"

//カメラを管理するクラス
class Camera : public IGameObject
{
	D3DXVECTOR3 target_;	//カメラの焦点
	bool IsReset_;

public:
	//コンストラクタ
	Camera(IGameObject* parent);

	//デストラクタ
	~Camera();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//カメラを初期位置に戻す
	//Direct3Dのソースまんま持ってきたから雑
	void ResetCamera();

	void SetTarget(D3DXVECTOR3 target)
	{
		target_ = target;
	}

};