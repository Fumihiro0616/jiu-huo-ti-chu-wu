#pragma once
#include "../Engine/Global.h"
#include "../Engine/Camera.h"

const int JUIGE_NUM = 3;	//判定の個数

enum GAMMODE
{
	BATTING_MODE,
	WATCHING_MODE,
	SHOW_RESULT,
	RESET
};

//判定用の列挙型
enum JUDGELIST
{
	ISFOUL,
	ISHIT,
	ISHOMERUN
};

class Camera;
class PlayScene;

class Ball : public IGameObject
{
	int hModel_;	//モデル番号を入れるヘッダー
	int modelHandle_[3];

	float ballacce_;
	float h_;

	bool isHomeRun_;	//ホームランかどうか
	bool isFoul_;		//ファウルかどうか
	bool isHit_;		//ヒットかどうか

	bool judgeList_[JUIGE_NUM];	//判定のフラグリスト
						//[0] ... ファウル 
						//[1] ... ヒット
						//[2] ... ホームラン


	float t_;			//時間
	float acceX_;		//Xへの加速度
	float y_;			//Y軸
	float acceY_;		//Yへの加速度

	float distZ_;		//Z軸の距離
	float distX_;		//X軸の距離
	int dist_;			//2つの距離
	float angle_;		//角度

	float ballAcceX_;	//ボールのX軸への移動値

	D3DXVECTOR3 playerRotate_;		//バットの角度
	D3DXVECTOR3 obliquePosition_;	//斜方投射した結果の座標

	D3DXVECTOR3 batNormal_;			//ボールがバットに当たった時の法線の向き

	D3DXVECTOR3 vecBall;

	PlayScene* pPlay;
	Camera* pCamera_;
	GAMMODE gameMode_;
	IGameObject* pParent_;

	D3DXMATRIX matRad_;

public:
	Ball(IGameObject* parent);
	~Ball();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	void CalcBallPos();

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void OnCollision(IGameObject* target) override;

	D3DXVECTOR3 Bound(D3DXVECTOR3 inVec);

	void SetPlayerRotate(D3DXVECTOR3 rotate) { playerRotate_ = rotate; };
	int GetGameMode() { return gameMode_; };
	void SetProjection_Angle(float angle) { angle_ = angle; };
};

