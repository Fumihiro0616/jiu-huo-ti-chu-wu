#pragma once
#include "../Engine/Global.h"
#include "../Engine/Collider/BoxCollider.h"
#include "../Engine/Collider/CylinderCollider.h"

class Player : public IGameObject
{
	BoxCollider* pCollider_;
	CylinderCollider* pCyliCollider_;
	int hModel_;

public:
	Player(IGameObject* parent);
	~Player();

	//初期化
	void Initialize() override;
	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void OnCollision(IGameObject* target) override;
	float GetPositionZ() { return position_.z; };
};

