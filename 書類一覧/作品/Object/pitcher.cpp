#include "Pitcher.h"

Pitcher::Pitcher(IGameObject* parent)
	: IGameObject(parent, "Pitcher"), IsBoolAppear_(false),ball_cnt_(0)
{
}

Pitcher::~Pitcher()
{
}

void Pitcher::Initialize()
{
}

void Pitcher::Update()
{
	//ボールが消えているときのみ球を投げる
	if (IsBoolAppear_ == false)
	{
		pBall = CreateGameObject<Ball>(this);
		IsBoolAppear_ = true;
	}

	//ボールが存在していたら他の処理は行わない
	if (IsBoolAppear_ == true)
	{
		if (pBall->GetGameMode() != WATCHING_MODE)
			pBall->SetPlayerRotate(playerRatate_);
	}
}

void Pitcher::Draw()
{
}

void Pitcher::Release()
{
}
