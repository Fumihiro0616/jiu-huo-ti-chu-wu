#include "Player.h"
#include "../Engine/Model.h"
#include "../Engine/SendData.h"

//定数
const float OVERANGLE = 90.0f; 	  //バットを振る上限角度
const float SWING_X = 5.0f;		  //バットをX軸に振る速度
const float SWING_Y = 10.0f;	  //バットをY軸に振る速度
const float INIT_ANGLE = 0.0f;    //バットの初期角度
const float MOVING_VALUE = 0.6f;  //移動値

Player::Player(IGameObject* parent)
	: IGameObject( parent, "Player" ), hModel_(-1)
{
}


Player::~Player()
{
}

void Player::Initialize()
{
	hModel_ = Model::Load("Data/Bat2.fbx");

	//ここのX軸の変更がうまく対応されない
	position_ = D3DXVECTOR3( 5.0f, 1.0f, -2.0f );

	//円柱の当たり判定( ボックスと定義方法が異なるため調整必須 )
	pCyliCollider_ = new CylinderCollider(

		//Maya上での原点とはずれているので調整を入れる
		//これを行わないと始点と終点が2.0ずれる
		D3DXVECTOR3(2.0f, 0.0f, 0.0f),		//始点
		D3DXVECTOR3(-8.0f, 0.0f, 0.0f),		//終点
		0.6f								//半径
	);
	AddCollider( pCyliCollider_ );
}

void Player::Update()
{
	//バッティングの状態のときのみバットを動かせる
	if (SendData::GetGameMode() == 0)
	{
		//Eキーでバットを振る
		if (Input::IsKey(DIK_E) && rotate_.y < OVERANGLE)
		{
			rotate_.y += SWING_Y;	//1秒で600°
			rotate_.z -= SWING_X;	//1秒で300°
		}

		else
		{
			rotate_.y = INIT_ANGLE;
			rotate_.z = INIT_ANGLE;
		}

		//横移動。範囲は未指定
		if (Input::IsKey(DIK_A))
		{

			position_.x -= MOVING_VALUE;
		}

		if (Input::IsKey(DIK_D))
		{
			position_.x += MOVING_VALUE;
		}
	}
}

void Player::Draw()
{
	//モデル表示
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

void Player::Release()
{
}

void Player::OnCollision(IGameObject * target)
{
	
}
