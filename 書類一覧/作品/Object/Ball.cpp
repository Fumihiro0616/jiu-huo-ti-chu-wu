#include "Ball.h"
#include "../Engine/Model.h"
#include "../Engine/Collider/Collider.h"
#include "../Engine/Collider/SphereCollider.h"
#include "Pitcher.h"
#include "../Engine/SendData.h"
#include "../Scene/PlayScene.h"
#include "../Engine/Image.h"
#include "../Engine/Audio.h"

//ホームランまで飛ぶ打球速度一覧
//( 空気抵抗は無視されているため実際とは相違 )
//時速140km 〜 ... 届く
//〜 時速135km ... 届かない

//秒速変換( 5km刻みで記載 )
//時速150km ... 41.6667f
//時速145km ... 40.2778f
//時速140km ... 38.8889f
//時速135km ... 37.5556f
//時速130km ... 36.1111f

//定数
const float G				 = 9.80665f;	//重力加速度
const float PER_SECOND		 = 41.6667f;	//打球速度(現在は150[km/h])
const float FRAME_PER_SECOND = 0.017f;		//1/60秒
const float RANGE_CORRECTION = 10.0f;		//[m]を[cm]に補正する
const float BALL_SPEED       = 1.5f;		//ボールの速度。視覚的に見やすくするために大幅に速度を落としている
const float GROUND_POS		 = 0.0f;		//地面の座標
const float BALL_RADIUS		 = 0.4f;		//ボールの半径
const int STRIKE_LINE		 = -20;			//ストライクのZ軸
const int MOUND_DIST		 = 180;			//マウンド距離

Ball::Ball(IGameObject * parent) :
	IGameObject(parent, "Ball"), ballacce_(BALL_SPEED),gameMode_(BATTING_MODE),
	obliquePosition_(D3DXVECTOR3(0, 0, 0)), isHomeRun_(false)
{
	pParent_ = parent;

	position_.z = MOUND_DIST;	//マウンドの距離は18mとして設定
	position_.y = 2.0f;			//高さは人が投げることを想定
	
	//判定リストを初期化
	for (int i = 1; i < sizeof(judgeList_); i++)
		judgeList_[i] = false;
}

Ball::~Ball()
{
}

void Ball::Initialize()
{
	//プレイシーンからステージ情報を取得
	pPlay = (PlayScene*)(pParent_->GetParent());
	modelHandle_[ISFOUL] = pPlay->Get_FoulZone_ModelHandle();
	modelHandle_[ISHIT] = pPlay->Get_HitZone_ModelHandle();
	modelHandle_[ISHOMERUN] = pPlay->Get_HomeRunZone_ModelHandle();

	//投げた数を増やす
	pPlay->CountBall();

	//コライダー作成
	SphereCollider* pCollider = new SphereCollider(
		D3DXVECTOR3(0, 0, 0), BALL_RADIUS);
	AddCollider( pCollider );

	//モデルロード
	hModel_ = Model::Load("Data/Ball.fbx");

	//ボールにランダムなX軸の移動値を生成
	//範囲は0.05 〜 -0.05
	ballAcceX_ = (float)(( rand() % 30 ) - 15 ) / 300;
}

void Ball::Update()
{
	//レイキャスト用のデータ
	//それぞれのゲームモードで使うため、静的にしておく
	static RayCastData data_;
	pPlay->SetGameMode( gameMode_ );

	//ゲームの状態
	switch (gameMode_)
	{
	//プレイヤーが操作する状態
	case BATTING_MODE:
		//ボールを投げた後の挙動
		position_.z -= ballacce_;
		position_.x += ballAcceX_;

		//空振ったら消すようにする
		if ( position_.z < STRIKE_LINE )
		{
			((Pitcher*)pParent_)->SetIsBoolAppear(false);
			KillMe();
		}
		break;

	//俯瞰視点でボールを追う
	case WATCHING_MODE:
		CalcBallPos();	//ボールの位置計算

		//床に落ちたら判定を行う
		if (position_.y <= GROUND_POS)
		{
			//レイを作成
			data_.orig = D3DXVECTOR3(position_.x, position_.y + 5.0f, position_.z);		//レイを飛ばす原点
			data_.dir = D3DXVECTOR3(0, -1, 0);											//下に飛ばす
			data_.hit = false;															//判定を初期化

			//全部1個ずつ確認しておく
			for (int i = ISFOUL; i <= ISHOMERUN; i++)
			{
				Model::RayCast(modelHandle_[i], data_);

				//当たってたらその判定をtrueにする
				if (data_.hit)
					judgeList_[i] = true;

				//当たってなかったらその判定をfalseにする
				else if (!data_.hit)
					judgeList_[i] = false;
			}

			gameMode_ = SHOW_RESULT;
		}

		break;

	//結果表示状態
	case SHOW_RESULT:
		//Z軸はプレイヤーの座標を参照
		distZ_ = pow(position_.z - ( -2.0f ), 2);
		distX_ = pow(position_.x, 2);
		dist_ = (int)sqrt(distZ_ + distX_) / 10;

		for ( int i = ISFOUL; i <= ISHOMERUN; i++ )
		{
			if (judgeList_[i])
			{
				//結果を表示
				//0 : ファウル
				//1 : ヒット
				//2 : ホームラン

				pPlay->SetJudge(i);
				pPlay->SetBallDist(dist_);

				//ホームランだったら歓声を出し、プレイシーンにあるホームランのカウント数を増やす
				if (i == ISHOMERUN)
				{
					Audio::Play("withHomerun");
					pPlay->CountHomeRun();
				}
				break;
			}
		}
		
		//判定リストを初期化
		for (int i = 1; i < sizeof(judgeList_); i++)
			judgeList_[i] = false;

		//スペースで歓声を止めて次に打数に進む
		if (Input::IsKeyDown(DIK_SPACE))
		{
			Audio::Stop("withHomerun");
			gameMode_ = RESET;
		}

		break;

	case RESET:
		//カメラをもとの位置に戻す
		pCamera_->ResetCamera();

		//死んだことをピッチャーに教えてあげないとエラーになる
		((Pitcher*)pParent_)->SetIsBoolAppear(false);
		pPlay->SetJudge(-1);
		KillMe();
		break;
	}

	SendData::SetGameMode(gameMode_);
}

void Ball::CalcBallPos()
{
	//ここで行う計算はあらかじめ情報から飛ぶ距離を算出し、
	//そこから時間を加味した移動量をpositionに入れる。
	//コライダーで求めたラジアンを度に変換し、投射角度とする
	angle_ = SendData::GetCollisionAngle();
	angle_ = (float)(angle_ * (180 / 3.14));

	//ボールを投射する際のY軸の角度をバットの角度から決める
	D3DXMatrixRotationY(&matRad_, D3DXToRadian(playerRotate_.y));

	//時間計測-----------------
	t_ += FRAME_PER_SECOND;
	//-------------------------

	//X軸計算-------------------------------------------
	acceX_ = PER_SECOND * cos(D3DXToRadian(angle_)) * t_;
	if (position_.y > GROUND_POS)	//地面より下には行かないように
		obliquePosition_.z = acceX_ * RANGE_CORRECTION;
	//--------------------------------------------------

	//Y軸計算---------------------------------------------------------------
	y_ = PER_SECOND * sin(D3DXToRadian(angle_)) * t_ - ((G * pow(t_, 2) / 2));
	acceY_ = PER_SECOND * sin(D3DXToRadian(angle_)) - G * t_;
	obliquePosition_.y = y_ * RANGE_CORRECTION;
	//----------------------------------------------------------------------

	//斜方投射したときの座標をバットの回転に合わせる
	D3DXVec3TransformCoord(&position_, &obliquePosition_, &matRad_);
}

void Ball::Draw()
{
	//モデル表示
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

void Ball::Release()
{
}

void Ball::OnCollision(IGameObject * target)
{
	//効果音を出す(バットの当たった音)
	Audio::Play("batHit");

	//打ったら俯瞰視点に切り替える
	gameMode_ = WATCHING_MODE;
	pCamera_ = CreateGameObject<Camera>(this);

	//バットの法線を受け取る
	batNormal_ = SendData::GetNormal();

	//カメラ作成
	//俯瞰視点
	pCamera_->SetPosition(D3DXVECTOR3(0, 20, -20));
	pCamera_->SetTarget(D3DXVECTOR3(0, 1, 0));
}