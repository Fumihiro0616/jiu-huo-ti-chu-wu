#pragma once

//プレイシーンを管理するクラス
#include "../Engine/Global.h"
#include "../Object/Player.h"
#include "../Object/pitcher.h"
#include "../Engine/Camera.h"

const int BALL_LIMIT = 15;
const int MODEL_MAX = 3;
const int NUMBER_MAX = 10;

class PlayScene : public IGameObject
{
	int stadiumModel_;			//スタジアムモデル
	int foul_Zone_Model_;		//ファールゾーンモデル
	int hit_Zone_Model_;		//ヒットゾーンモデル
	int homerun_Zone_Model_;	//ホームランゾーンモデル

	int homeRunLogPict_;		//ホームランログ画像
	int hitLogPict_;			//ヒットログ画像
	int foulLogPict_;			//ファールログ画像
	int countBoardPict_;		//カウントボード画像
	int meterText_;				//「m」のテキスト

	int numberList_[NUMBER_MAX];//数値リスト
	int picHandle_[MODEL_MAX];	//画像のハンドル
	int modelHandle_[MODEL_MAX];//モデルのハンドル

	int ball_cnt_;				//ボールの残球数
	int homeRun_cnt_;			//ホームランの数

	int judgeHandle_;			//判定の結果を受けとる
	int gameMode_;				//ゲームの状態
	int dist_;					//距離

	int maxDist_;				//最高飛距離(最終的にリザルトで表示させる)

	//行列関係
	D3DXMATRIX mat_;
	D3DXMATRIX matX_;
	D3DXMATRIX matSecondDigit_;
	D3DXMATRIX distMat_;
	D3DXMATRIX distDigitMat_;
	D3DXMATRIX matMeter_;

	Player *pPlayer_;			//プレイヤーのオブジェクト
	Pitcher *pPit_;				//ピッチャーのオブジェクト

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);
	
	//PlayScene();
	~PlayScene();
	
	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//判定結果を出す画像番号をもらう
	void SetJudge(int handle) { judgeHandle_ = handle; };

	//今のゲーム状態を知る
	void SetGameMode(int gameMode) { gameMode_ = gameMode; };

	//ボールの飛距離を知る
	void SetBallDist(int dist) { dist_ = dist; };

	void CountBall() { ball_cnt_--; };			//残球数をカウント
	void CountHomeRun() { homeRun_cnt_++; };	//ホームラン数をカウント

	//モデル番号を渡す
	int Get_FoulZone_ModelHandle() { return modelHandle_[0]; };
	int Get_HitZone_ModelHandle() { return modelHandle_[1]; };
	int Get_HomeRunZone_ModelHandle() { return modelHandle_[2]; };
};

