#pragma once
#include "../Engine/Global.h"
#include "../Engine/Input.h"

class TitleScene : public IGameObject
{
	int titlePict_;				//タイトル画面の画像
	int controlText_;			//「PRESS ENTER START」のテキスト
	int controlTextNoBorder_;	//「PRESS ENTER START」のテキストの境界線がないバージョン

	D3DXMATRIX controlMat_;

	
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(IGameObject* parent);

	//PlayScene();
	~TitleScene();


	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};

