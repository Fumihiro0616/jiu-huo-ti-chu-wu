#pragma once
#include <list>
#include "../Engine/Global.h"
#include "../Engine/Input.h"


class ResultScene : public IGameObject
{
	int recvData_;				//スコア受け取る用のデータ
	int recvDistData_;			//最高飛距離を受け取る用のデータ
	int scenePic_;				//背景画像用のハンドル番号
	int scoreLogPic_;				//ロゴ用のハンドル番号
	int rankLogPic_;				//ロゴ用のハンドル番号
	int numberList_[10];			//数値リスト
	int meterText_;				//「m」のテキスト
	int controlText_;				//「PRESS ENTER START」のテキスト
	int controlTextBorderLess_;	//「PRESS ENTER START」のテキストの境界線がないバージョン
	std::list<int> scoreDataList_;	//スコアを格納するリスト( 中身の量が不定なため配列ではなくリスト )

	//行列関係
	D3DXMATRIX matMeter_;			//「m」のテキストの位置行列
	D3DXMATRIX distDigitMat_;		//数値の桁をずらすときの移動幅
	D3DXMATRIX distMat_;			//飛距離の数値の位置行列
	D3DXMATRIX matScore_;			//スコアの数値の位置行列
	D3DXMATRIX matRank_;			//ランクの数値の位置行列
	D3DXMATRIX matScoreLog_;		//「SCORE」のテキストの位置行列
	D3DXMATRIX matRankLog_;			//「RANK」のテキストの位置行列
	D3DXMATRIX controlMat_;
	
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	ResultScene(IGameObject* parent);

	//PlayScene();
	~ResultScene();

	//初期化
	void Initialize() override;

	void OpenFile();

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
	void CloseDB();
	void Add_Data();
	void ExtractionData();
};

