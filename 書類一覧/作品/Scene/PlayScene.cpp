#include "PlayScene.h"
#include "../Engine/Model.h"
#include "../Engine/Camera.h"
#include "ResultScene.h"
#include "../Engine/Image.h"
#include "../Engine/SendData.h"
#include "../Engine/Audio.h"

const int EMD_GAME_BALL = 0;		//ゲームを終わらせるときの球数
const int BALL_MAX = 15;			//球数の上限
const int STRING_BUFFER = 32;		//文字列格納

const int TWO_DIGITS = 10;		//10の位の数値を取得
const int THREE_DIGITS = 100;		//100の位の数値を取得

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent) :
	//ここでIGameObjectのコンストラクタを呼んでいる
	IGameObject(parent, "PlayScene"), stadiumModel_(-1),
	judgeHandle_(-1), ball_cnt_(BALL_MAX), homeRun_cnt_(0),gameMode_(BATTING_MODE), maxDist_(0)
{
}

PlayScene::~PlayScene()
{
	
}

//初期化
void PlayScene::Initialize()
{
	//Playerを作成
	pPlayer_ = CreateGameObject<Player>(this);
	pPit_ = CreateGameObject<Pitcher>(this);

	//モデルの名前
	std::string modelName[MODEL_MAX] =
	{
		"Foul_Zone",
		"Hit_Zone",
		"HomeRun_Zone"
	};

	//モデルをロード
	for (int i = 0; i < MODEL_MAX; i++)
	{
		modelHandle_[i] = Model::Load("data/" + modelName[i] + ".fbx");
		assert(modelHandle_[i] >= 0);
	}

	//画像の名前
	std::string picName[MODEL_MAX] = 
	{
		"FOUL_LOG",
		"HIT_LOG",
		"HOMERUN_LOG"
	};

	//画像をロード
 	for ( int i = 0; i < MODEL_MAX; i++ )
	{
		picHandle_[i] = Image::Load( "data/" + picName[i] + ".png" );
		assert(picHandle_[i] >= 0);
	}
	
	//数値変換用の枠
	char num[STRING_BUFFER];

	//スコアで使う数値の画像をロード
	for ( int i = 0; i < NUMBER_MAX; i++ )
	{
		sprintf_s( num, "data/numberlist/Pic_%d.png", i );
		numberList_[i] = Image::Load(num);
		assert(numberList_[i] >= 0);
	}

	meterText_ = Image::Load("data/numberlist/Pic_meter.png");
	assert(meterText_ >= 0);

	//カウント表示の枠をロード
	countBoardPict_ = Image::Load( "data/Count_Board.png" );
	assert( countBoardPict_ >= 0);

 
	//行列の設定
	D3DXMatrixTranslation(&mat_, 0, 60, 0);
	D3DXMatrixTranslation(&matX_, 180, 0, 0);
	D3DXMatrixTranslation(&matSecondDigit_, -20, 0, 0);
	D3DXMatrixTranslation(&distMat_, 370, 180, 0);
	D3DXMatrixTranslation(&distDigitMat_, -20, 0, 0);
	D3DXMatrixTranslation(&matMeter_, 30, 0, 0);

	//スタジアムのざわめきの音を出す
	Audio::Play("buzz");
}

//更新
void PlayScene::Update()
{
	//常に最高飛距離を保持しておく
	if (maxDist_ < dist_) maxDist_ = dist_;

	//15回球を投げたら終了
	if ( ball_cnt_ == EMD_GAME_BALL )
	{
		//リザルトシーンに移行
		//最高飛距離を送らなければいけない
		Audio::Stop("buzz");
		SendData::SetDistData( maxDist_ );
		SendData::SetScoreData( homeRun_cnt_ );
		SceneManager::ChangeScene(SCENE_ID_RESULT);
	}
}

//描画
void PlayScene::Draw()
{

	//ゲームの状態で表示するものを変更
	switch ( gameMode_ )
	{
	//結果を出している状態
	case SHOW_RESULT:
		//結果を出す
		Image::SetMatrix(picHandle_[judgeHandle_], worldMatrix_);
		Image::Draw(picHandle_[judgeHandle_]);

		//飛距離表示(3桁)
		//3桁のとき
		if (dist_ >= THREE_DIGITS)
		{
			int hundreads_place = dist_ / THREE_DIGITS;		//100の位
			int twoDigit = dist_ % THREE_DIGITS;		

			int tens_place = twoDigit / TWO_DIGITS;			//10の位
			int one_place  = twoDigit % TWO_DIGITS;			//1の位

			Image::SetMatrix(numberList_[hundreads_place], worldMatrix_ * distMat_ * distDigitMat_ * distDigitMat_);
			Image::Draw(numberList_[hundreads_place]);

			Image::SetMatrix(numberList_[tens_place], worldMatrix_ * distMat_ * distDigitMat_);
			Image::Draw(numberList_[tens_place]);

			Image::SetMatrix(numberList_[one_place], worldMatrix_ * distMat_);
			Image::Draw(numberList_[one_place]);
		}

		//2桁のとき
		else if (dist_ >= TWO_DIGITS)
		{
			int tens_place = dist_ / TWO_DIGITS;		//10の位
			int one_place  = dist_ % TWO_DIGITS;		// 1の位

			Image::SetMatrix(numberList_[tens_place], worldMatrix_ * distMat_ * distDigitMat_);
			Image::Draw(numberList_[tens_place]);

			Image::SetMatrix(numberList_[one_place], worldMatrix_ * distMat_);
			Image::Draw(numberList_[one_place]);
		}

		//1桁のとき
		else
		{
			Image::SetMatrix(numberList_[dist_], worldMatrix_ * distMat_);
			Image::Draw(numberList_[dist_]);
		}

		Image::SetMatrix(meterText_, worldMatrix_ * distMat_ * matMeter_);
		Image::Draw(meterText_);

		break;
	}
	
	//常時出しているもの
	//カウント表示枠
	Image::SetMatrix( countBoardPict_, worldMatrix_ );
	Image::Draw(countBoardPict_);

	//2桁のとき
	if ( ball_cnt_ >= TWO_DIGITS)
	{
		int tens_place = ball_cnt_ / TWO_DIGITS;	//10の位
		int one_place = ball_cnt_ % TWO_DIGITS;		// 1の位

		//残級数側の文字
		Image::SetMatrix(numberList_[tens_place], worldMatrix_ * matX_ * matSecondDigit_);
		Image::Draw(numberList_[tens_place]);

		Image::SetMatrix(numberList_[one_place], worldMatrix_ * matX_);
		Image::Draw(numberList_[one_place]);
	}

	//1桁のとき
	else
	{
		//残級数側の文字
		Image::SetMatrix(numberList_[ball_cnt_], worldMatrix_ * matX_);
		Image::Draw(numberList_[ball_cnt_]);
	}

	//ホームラン数側の文字
	Image::SetMatrix(numberList_[homeRun_cnt_], worldMatrix_ * mat_ * matX_ );
	Image::Draw(numberList_[homeRun_cnt_]);

	for ( unsigned int i = 0; i < MODEL_MAX; i++ )
	{
		Model::SetMatrix(modelHandle_[i], worldMatrix_);
		Model::Draw(modelHandle_[i]);
	}
}

//開放
void PlayScene::Release()
{
	
}