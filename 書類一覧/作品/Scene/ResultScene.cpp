#include <iostream>
#include "ResultScene.h"
#include "TitleScene.h"
#include "../Sqlite/sqlite3.h"
#include "../Engine/SendData.h"
#include "../Engine/Image.h"

using namespace std;
const int STRING_BUFFER = 32;		//文字列格納
const int RANK_MAX = 5;			//ランキング数
const int HALF_SECOND = 30;

const int PIC_MAX = 9;

const int TWO_DIGITS = 10;		//10の位の数値を取得
const int THREE_DIGITS = 100;		//100の位の数値を取得
sqlite3 *pDB_ = NULL;

ResultScene::ResultScene(IGameObject * parent) : recvData_(0)
{
}

ResultScene::~ResultScene()
{
}

void ResultScene::Initialize()
{
	//画像をロード
	scenePic_ = Image::Load( "Data/Result_Scene.png" );
	assert( scenePic_ >= 0 );

	scoreLogPic_ = Image::Load("Data/Score_LOG.png");
	assert(scoreLogPic_ >= 0);

	rankLogPic_ = Image::Load("Data/Rank_LOG.png");
	assert(rankLogPic_ >= 0);

	controlText_ = Image::Load("Data/PRESS_ENTER_TO_RETURN_HOME.png");
	assert(controlText_ >= 0);

	controlTextBorderLess_ = Image::Load("Data/PRESS_ENTER_TO_RETURN_HOME(BORDERLESS).png");
	assert(controlText_ >= 0);

	char t[STRING_BUFFER];

	//スコアで使う数値の画像をロード
	for (int i = 0; i <= PIC_MAX; i++)
	{
		sprintf_s(t, "data/numberlist/Pic_%d.png", i);
		numberList_[i] = Image::Load(t);
		assert(numberList_[i] >= 0);
	}

	meterText_ = Image::Load("data/numberlist/Pic_meter.png");
	assert(meterText_ >= 0);

	//行列設定---------------------------------------------
	D3DXMatrixTranslation(&matMeter_, 450, 490, 0);
	D3DXMatrixTranslation(&distDigitMat_, -25, 0, 0);
	D3DXMatrixTranslation(&distMat_, 400, 490, 0);
	D3DXMatrixTranslation(&matScore_, 0, 0, 0 );
	D3DXMatrixTranslation(&matRank_,  0, 0, 0 );
	D3DXMatrixTranslation(&matScoreLog_, 415, 100, 0);
	D3DXMatrixTranslation(&matRankLog_, 265, 100, 0);
	D3DXMatrixTranslation(&controlMat_, 160, 550, 0);
	//-----------------------------------------------------

	//ファイルを開く
	OpenFile();

	//データを追加
	Add_Data();

	//データを抽出
	ExtractionData();

}

void ResultScene::Update()
{
	//Enterキーで遷移
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//タイトルシーンに移行
		CloseDB();	//データベースを閉じる
		SceneManager::ChangeScene(SCENE_ID_TITLE);
	}
}

void ResultScene::Draw()
{
	//表示部
	Image::SetMatrix( scenePic_, worldMatrix_ );
	Image::Draw( scenePic_ );
	
	Image::SetMatrix( scoreLogPic_, worldMatrix_ * matScoreLog_ );
	Image::Draw( scoreLogPic_ );

	Image::SetMatrix(rankLogPic_, worldMatrix_ * matRankLog_);
	Image::Draw(rankLogPic_);

	int index = 0;	//1ランクごとに行列を移動させる必要がある
	auto itr = scoreDataList_.begin();

	//ScoreDataの中身を表示する
	for ( int i = 0; i < RANK_MAX; i++ )
	{
		if (itr == scoreDataList_.end()) break;
		index++;

		//配置設定
		D3DXMatrixTranslation( &matScore_, 450, 50 * (float)index + 100, 0 );
		D3DXMatrixTranslation( &matRank_,  300, 50 * (float)index + 100, 0 );

		//スコア表示
		Image::SetMatrix(numberList_[*itr], worldMatrix_ * matScore_);
		Image::Draw(numberList_[*itr]);

		//ランク表示
		Image::SetMatrix(numberList_[index], worldMatrix_ * matRank_);
		Image::Draw(numberList_[index]);

		itr++;
	}

	//最高飛距離出力-----------------------------------------------------------
	Image::SetMatrix(meterText_, worldMatrix_ * matMeter_);
	Image::Draw(meterText_);
	
	//3桁のとき
	if (recvDistData_ >= THREE_DIGITS)
	{
		int hundreads_place = recvDistData_ / THREE_DIGITS;		//100の位
		int twoDigit = recvDistData_ % THREE_DIGITS;

		int tens_place = twoDigit / TWO_DIGITS;						//10の位
		int one_place = twoDigit % TWO_DIGITS;							//1の位

		Image::SetMatrix(numberList_[hundreads_place], worldMatrix_ * distMat_ * distDigitMat_ * distDigitMat_);
		Image::Draw(numberList_[hundreads_place]);

		Image::SetMatrix(numberList_[tens_place], worldMatrix_ * distMat_ * distDigitMat_);
		Image::Draw(numberList_[tens_place]);

		Image::SetMatrix(numberList_[one_place], worldMatrix_ * distMat_);
		Image::Draw(numberList_[one_place]);
	}

	//2桁のとき
	else if (recvDistData_ >= TWO_DIGITS)
	{
		int tens_place = recvDistData_ / TWO_DIGITS;	//10の位
		int one_place = recvDistData_ % TWO_DIGITS;		// 1の位

		Image::SetMatrix(numberList_[tens_place], worldMatrix_ * distMat_ * distDigitMat_);
		Image::Draw(numberList_[tens_place]);

		Image::SetMatrix(numberList_[one_place], worldMatrix_ * distMat_);
		Image::Draw(numberList_[one_place]);
	}

	//1桁のとき
	else
	{
		Image::SetMatrix(numberList_[recvDistData_], worldMatrix_ * distMat_);
		Image::Draw(numberList_[recvDistData_]);
	}

	Image::SetMatrix(meterText_, worldMatrix_ * distMat_ * matMeter_);
	Image::Draw(meterText_);

	//-------------------------------------------------------------------------

	//「PRESS ENTER TO RETURN HOME」のテキストを0.5秒ごとに入れ替え表示する
	static int time = 0;
	static int cnt = 0;

	if (cnt % 2 == 0) {
		Image::SetMatrix(controlText_, worldMatrix_ * controlMat_);
		Image::Draw(controlText_);
	}

	else
	{
		Image::SetMatrix(controlTextBorderLess_, worldMatrix_ * controlMat_);
		Image::Draw(controlTextBorderLess_);
	}

	time++;
	//0.5秒経ったら画像を入れ替え
	if (time > HALF_SECOND)
	{
		cnt++;
		time = 0;
	}
}

//データベースを開く
void ResultScene::OpenFile()
{
	//データベースのファイル名(固定)
	const char* fileName = "data/ScoreData.db";

	//データベースのオープン
	//第一引数 ... ファイル名( const char* )
	//第二引数 ... データベース型のオブジェクト( sqlite3 )
	int err = sqlite3_open(fileName, &pDB_);

	//ちゃんと開けているか
	if (err != SQLITE_OK)
	{
		//エラー処理
	}

	char* errMsg = NULL;	//テーブルをいれる箱
	int errTable = 0;

	//テーブル作成( 中身にはスコア用のint型のみ )
	//第一引数 ... sqlite3型
	//第二引数 ... 命令文 デーブル作成。同じのがある場合はスキップされる
	//第三引数 ... callback関数( NULL )
	//第四引数 ... callback関数( NULL )
	//第五引数 ... メッセージ受け取るchar*
	errTable = sqlite3_exec(pDB_,
		"CREATE TABLE IF NOT EXISTS myTable"
		"(score INTEGER, dist INTEGER)",
		NULL, NULL, &errMsg);

	//ちゃんとテーブルを作れているか
	if (errTable != SQLITE_OK)
	{
		sqlite3_free(errMsg);	//解放処理
	}
}

//データベースを閉じる
void ResultScene::CloseDB()
{
	//データベースをクローズ
	//第一引数 ... データベース型のオブジェクト( sqlite3 )
	int err = sqlite3_close(pDB_);

	//ちゃんと閉じれているか
	if (err != SQLITE_OK)
	{
		//エラー処理

		/* その後の処理 */
	}
}

//データベースを追加する
void ResultScene::Add_Data()
{
	//ステートメントオブジェクト
	//( SQL分をデータベースに送るときに使うもの )
	sqlite3_stmt *pStmt = NULL;

	//ステートメントの用意
	int err = sqlite3_prepare_v2(pDB_,
		"INSERT INTO myTable (score, dist) VALUES (?, ?)",
		128, &pStmt, NULL);

	//ちゃんと入っているか
	if (err != SQLITE_OK) {
		//エラー処理
	}

	else {
		//入力用のデータ( ここにSendDataからデータを送る )
		unsigned int inputScoreData = SendData::GetScoreData();
		int inputDistData = SendData::GetDistData();

		//SQL文に数値を入れる( int型バージョン )
		sqlite3_bind_int(pStmt, 1, inputScoreData);
		sqlite3_bind_int(pStmt, 2, inputDistData);

		unsigned int inputData = SendData::GetScoreData();

		//SQL文に数値を入れる( int型バージョン )
		//第一引数 ... ステートメント
		//第二引数 ... SQL文にある?の何個目から差し替えるか( 基本1 )
		//第三引数 ... 値
		sqlite3_bind_int(pStmt, 1, inputData);
		
		//SQL文を実行させる( 引数はステートメント )
		while (SQLITE_DONE != sqlite3_step(pStmt))
		{
			//エラー処理
		}
	}

	sqlite3_finalize(pStmt);
}

//データを抽出する
void ResultScene::ExtractionData()
{
	//ステートメントオブジェクト
	//( SQL分をデータベースに送るときに使うもの )
	sqlite3_stmt *pStmt = NULL;
	int errStmt_Re = 0;

	//スコア表示( 降順 )
	errStmt_Re = sqlite3_prepare_v2(pDB_,
		"SELECT score FROM myTable ORDER BY score DESC ", 64,
		&pStmt, NULL);

	if (errStmt_Re != SQLITE_OK) {
		//エラー処理
	}

	else {

		//5位まで出すようにする
		while (SQLITE_ROW == (errStmt_Re = sqlite3_step(pStmt)))
		{
			recvData_ = sqlite3_column_int(pStmt, 0);

			//そのスコアをリストに追加
			scoreDataList_.push_back(recvData_);
		}
		

		if (errStmt_Re != SQLITE_DONE)
		{
			//エラー処理
		}
	}

	//別途で格納されているデータベース内で一番の飛距離を取得する
	errStmt_Re = sqlite3_prepare_v2(pDB_,
		"SELECT MAX(dist) FROM myTable", 64,
		&pStmt, NULL);

	if (errStmt_Re != SQLITE_OK) {
		//エラー処理
	}

	else {

		while (SQLITE_ROW == (errStmt_Re = sqlite3_step(pStmt)))
		{
			//最高飛距離をデータベース内から取得する
			recvDistData_ = sqlite3_column_int(pStmt, 0);
		}


		if (errStmt_Re != SQLITE_DONE)
		{
			//エラー処理
		}
	}

	//ステートメントの解放
	sqlite3_finalize(pStmt);
}

void ResultScene::Release()
{
}