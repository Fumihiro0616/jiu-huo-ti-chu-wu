#pragma once
#include "../Engine/Global.h"

namespace SendData
{
	void SetCollisionAngle(float angle);
	float GetCollisionAngle();

	void SetScoreData(unsigned int score);
	unsigned int GetScoreData();

	void SetDistData(unsigned int dist);
	unsigned int GetDistData();

	void SetNormal(D3DXVECTOR3 normal);
	D3DXVECTOR3 GetNormal();

	void SetGameMode(int gamemode);
	int GetGameMode();

};