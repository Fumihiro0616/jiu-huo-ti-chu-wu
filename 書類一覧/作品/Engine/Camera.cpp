#include "Camera.h"

//コンストラクタ
Camera::Camera(IGameObject * parent)
	:IGameObject(parent, "Camera"),
	target_(D3DXVECTOR3(0, 0, 0)), IsReset_( false )
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化
void Camera::Initialize()
{
	//プロジェクション行列作成(途中で変えるものはないのでここにする)
	D3DXMATRIX proj;

	//D3DXMatrixPerspectiveFovLH ... プロジェクション行列を作成
	//第1引数 ... 行列
	//第2引数 ... 視野角(見える範囲)
	//第3引数 ... アスペクト比(矩形における長辺と短辺の比率。)
	//第4引数 ... 近クリッピング面。カメラの写す距離のはじめ(ある程度の距離はあったほうがいい)
	//第5引数 ... 遠クリッピング面。カメラの見える距離
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 0.5f, 1000.0f);

	//プロジェクション行列に変換
	Direct3D::pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}

//更新
void Camera::Update()
{
	//リセット処理を行われてなければ行う
	if (IsReset_ != true)
	{
		IGameObject::Transform();

		//ワールド座標とワールドターゲットを作成
		D3DXVECTOR3 worldPosition, worldTarget;

		//それらをworldmatrixを使って変形させる
		D3DXVec3TransformCoord(&worldPosition, &position_, &worldMatrix_);
		D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);

		//ビュー行列作成
		D3DXMATRIX view;

		//D3DXMatrixLookAtLH ... ビュー行列を作成
		//第1引数 ... 行列
		//第2引数 ... カメラの視点位置
		//第3引数 ... カメラの焦点
		//第4引数 ... カメラの上方向ベクトル。(0, 1, 0)基本コレ
		D3DXMatrixLookAtLH(&view, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));

		//ビュー行列に変換
		Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view);
	}
}

//描画
void Camera::Draw()
{
}

//開放
void Camera::Release()
{
}

//カメラをリセットする
void Camera::ResetCamera()
{
	//リセットしたことにする。
	//これを行わないとUpdateに入り、意味がなくなる
	IsReset_ = true;

	//ここの数値をDicrect3D.cppから持ってこれるようにする
	//カメラ
	D3DXMATRIX view;
	D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 5, -10), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));

	//ビュー行列に変換
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view);

	D3DXMATRIX proj;
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 0.5f, 3000.0f);

	//プロジェクション行列に変換
	Direct3D::pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}
