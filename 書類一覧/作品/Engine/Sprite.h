#pragma once
#include <d3dx9.h>
#include "Global.h"

class Sprite
{
	LPD3DXSPRITE       pSprite_;    //スプライト
	LPDIRECT3DTEXTURE9 pTexture_;   //テクスチャ

public:
	Sprite();
	~Sprite();

	//void Initialize(HWND hWnd);

	//スプライトとテクスチャの作成
	//引数pDevice : 
	//途中でpPictureの中身は変わらないのでconstをつけよう
	HRESULT Load( std::string fileName );

	//ゲーム画面の描画
	//ゲーム画面の表示の位置や回転などの行列
	void Draw(const D3DXMATRIX& matrix, RECT* rect);

	D3DXVECTOR2 GetTextureSize();

	//void Release();
};