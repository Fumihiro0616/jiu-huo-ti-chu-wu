#pragma once
#include "Global.h"
#include <fbxsdk.h>

class Fbx
{
	//ここの順番はフラグの定義順なので注意
	struct Vertex
	{

		D3DXVECTOR3 pos;

		//法線の情報(法線って英語でnormalらしい)
		D3DXVECTOR3 normal;

		//UV座標の情報
		D3DXVECTOR2 uv;
	};

	//バーテックスバッファ ... 用意した4つ分の頂点を入れるメモリを用意
	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;

	//インデックスバッファ
	LPDIRECT3DINDEXBUFFER9* ppIndexBuffer_;

	//テクスチャ
	LPDIRECT3DTEXTURE9* pTexture_;

	//マテリアルを入れる変数を追加する
	//※マテリアル ... CG用語におけるポリゴンの質感のこと
	D3DMATERIAL9*         pmaterial_;

	FbxManager*  pManager_;		//FBXファイルを開くためのマネージャー
	FbxImporter* pImporter_;	//ファイルを開くインポーター
	FbxScene*    pScene_;		//開いたファイルを管理するシーン

	int vertexCount_;			//頂点の数を入れる変数
	int polygonCount_;			//ポリゴンの数を入れる変数
	int indexCount_;			//インデックス数を入れる変数　※インデックス数はポリゴン数の3倍
	u_int materialCount_;
	int* polygonCountOfMaterial_;
	void CheckNode(FbxNode* pNode);
	void CheckMesh(FbxMesh* pMesh);

public:
	Fbx();
	~Fbx();

	void Load(const char* filename);
	void Draw(const D3DXMATRIX& matrix);
	void RayCast(RayCastData& data, const D3DXMATRIX& matrix);
};