#include "SendData.h"

namespace
{
	float angle_ = 0.0f;		//バットの回転情報
	unsigned int score_ = 0;	//スコアデータ
	unsigned int dist_ = 0;		//飛距離データ
	unsigned int gameMode_ = 0;	//ゲームの状態
	D3DXVECTOR3 normal_;		//法線データ
}

//角度を送る関数
void SendData::SetCollisionAngle(float angle)
{
	angle_ = angle;
}

//角度を受けとる関数
float SendData::GetCollisionAngle()
{
	return angle_;
}

//スコアを送る関数
void SendData::SetScoreData(unsigned int score)
{
	score_ = score;
}

//スコアを受けとる関数
unsigned int SendData::GetScoreData()
{
	return score_;
}

//距離を送る関数
void SendData::SetDistData(unsigned int dist)
{
	dist_ = dist;
}

//距離を受け取る関数
unsigned int SendData::GetDistData()
{
	return dist_;
}

//法線を送る関数
void SendData::SetNormal(D3DXVECTOR3 normal)
{
	normal_ = normal;
}

//法線を受け取る関数
D3DXVECTOR3 SendData::GetNormal()
{
	return normal_;
}

//ゲームの状態を送る関数
void SendData::SetGameMode(int gamemode)
{
	gameMode_ = gamemode;
}

//ゲームの状態を受け取る関数
int SendData::GetGameMode()
{
	return gameMode_;
}