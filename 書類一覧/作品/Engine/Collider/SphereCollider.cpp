//#include "BoxCollider.h"
#include "SphereCollider.h"

SphereCollider::SphereCollider(D3DXVECTOR3 center, float radius)
{
	center_ = center;
	radius_ = radius;
	type_ = COLLIDER_CIRCLE;
}

SphereCollider::~SphereCollider()
{
}

bool SphereCollider::IsHit(Collider* target)
{
	//当たったものがボックスだった時の当たり判定
	if (target->type_ == COLLIDER_BOX)
	{
		return Collision_OBBToSphere((BoxCollider*)target, this);
	}

	//当たったものが円柱だった時の当たり判定
	else if ( target->type_ == COLLIDER_CYLINDER )
	{
		int a = 0;
		return Collision_CylinderToSphere(this, (CylinderCollider*)target);
	}

	return false;
}
