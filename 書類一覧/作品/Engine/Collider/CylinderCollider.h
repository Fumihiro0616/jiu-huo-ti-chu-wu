#pragma once
#include "Collider.h"

class CylinderCollider : public Collider
{
	friend class Collider;

	//構成要素( 直線 )
	D3DXVECTOR3 center_;	//始点
	D3DXVECTOR3 dir_;		//方向ベクトル

	D3DXVECTOR3 origin_;	//原点
	D3DXVECTOR3 posE_;		//終点
	float radius_;			//半径

public:
	CylinderCollider( D3DXVECTOR3 center, D3DXVECTOR3 posE, float radius );
	~CylinderCollider();
	
	//方向ベクトルを算出
	D3DXVECTOR3 GetDirVec();
	bool IsHit( Collider* target );
};