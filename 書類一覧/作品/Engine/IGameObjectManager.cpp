#include "IGameObjectManager.h"
#include "IGameObject.h"
#include "Global.h"

#pragma comment(lib, "winmm.lib")

namespace
{
	//ルートオブジェクトクラスを作成
	//オブジェクトを探索する際に探す「頭」の部分としてとして使う
	class RootJob : public IGameObject
	{
	public:
		RootJob() : IGameObject(nullptr, "RootJob") {}
		~RootJob() {}
		void Initialize(){}
		void Update() {}
		void Draw() {}
		void Release() {}
	};

	//ルートオブジェクトを作成
	RootJob* pRootjob = nullptr;

	//更新処理
	//引数：obj	対象のオブジェクト
	void UpdateSub(IGameObject* obj)
	{

		//更新フラグがfalseなら終了
		if (!obj->IsEntered())
			return;

		//対象オブジェクトの更新処理
		obj->Update();
		obj->Transform();
		//obj->TransformCollider();

		//その子オブジェクトの更新処理
		auto list = obj->GetChildList();
		for (auto it = list->begin(); it != list->end();)
		{
			//まだ初期化してなかったら初期化する
			if (!(*it)->IsInitialized())
			{
				(*it)->SetInitialized();	//初期化フラグを立てる
				(*it)->Initialize();		//初期化
			}

			//更新処理
			UpdateSub(*it);

			//削除フラグが立ってたら削除
			if ((*it)->IsDead())
			{
				(*it)->Release();
				SAFE_DELETE(*it);
				it = list->erase(it);
			}
			else
			{
				//当たり判定
				(*it)->Collision(pRootjob);

				it++;
			}
		}
	}

	void DrawSub( IGameObject* obj )
	{
		//描画フラグがfalseなら終了
		if (!obj->IsVisibled())
			return;

		//対象オブジェクトの描画
		obj->Draw();

		//その子オブジェクトの描画処理
		auto list = obj->GetChildList();
		for (auto it = list->begin(); it != list->end(); it++)
		{
			DrawSub(*it);
		}
	}

	void ReleaseSub(IGameObject* obj)
	{
		//まずは子オブジェクトを開放
		auto list = obj->GetChildList();
		for (auto it = list->begin(); it != list->end(); it++) {
			ReleaseSub(*it);
		}

		//対象オブジェクトを開放
		obj->Release();
		SAFE_DELETE(obj);
	}
}

namespace IGameObjectManager
{
	//初期化
	void Initialize()
	{
		//ルートオブジェクト作成
		pRootjob = new RootJob;

		//シーンマネージャー作成
		CreateGameObject<SceneManager>(pRootjob);

	}

	void Update()
	{
		UpdateSub( pRootjob );
	}

	void Draw()
	{
		DrawSub( pRootjob );
	}

	void Release()
	{
		ReleaseSub( pRootjob );
	}

	IGameObject * FindChildObject(const std::string & name)
	{
		return pRootjob->FindChildObject(name);
	}
}
