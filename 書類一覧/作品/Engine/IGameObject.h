#pragma once

#include <list>
#include <d3dx9.h>
#include <list>
#include <string>


//クラスのプロトタイプ宣言
//使えるのは戻り値とかのみ、変数は作れない

//class RootJob : public IGameObject
//{
//public:
//	RootJob() : IGameObject(nullptr, "RootJob") {}
//	~RootJob() {}
//	void Initialize()
//	{
//		//シーンマネージャーを作成し、Rootjobの子とする
//		CreateGameObject<SceneManager>(this);
//	}
//	void Update() {}
//	void Draw() {}
//	void Release() {}
//};

class Collider;

//IGameObjectのIってInterferceのIだったらしい
class IGameObject
{
private:
	struct OBJECT_STATE
	{
		unsigned initialized : 1;	//初期化済みか
		unsigned entered : 1;		//更新するか
		unsigned visible : 1;		//描画するか
		unsigned dead : 1;			//削除するか
	};
	OBJECT_STATE state_;

protected:
	//純粋仮想関数は実態を持てないため、ポインタにしないといけない
	IGameObject* pParent_;					//親は誰だか
	std::list<IGameObject*> pChildlist_;	//子供(何人いるか不明なのでlist型を使おう)
	std::string name_;						//とりあえず文字列が入る型

	D3DXVECTOR3 position_;					//位置
	D3DXVECTOR3 rotate_;					//回転
	D3DXVECTOR3 scale_;						//拡大

	D3DXMATRIX  localMatrix_;				//親から見た行列
	D3DXMATRIX	worldMatrix_;				//全体から見た行列
	std::string objectName_;

	bool dead_;

	Collider* pCollider_;

	std::list<Collider*> colliderList_;

private:

public:
	IGameObject();
	IGameObject(IGameObject* parent);

	//stringは文字列型
	IGameObject(IGameObject* parent, const std::string& name);
	virtual ~IGameObject();

	//コンストラクタ(純粋仮想関数)//
	//初期化
	virtual void Initialize() = 0;

	//更新
 	virtual void Update() = 0;

	//描画
	virtual void Draw() = 0;

	//開放
	virtual void Release() = 0;

	void UpdateSub();
	void DrawSub();
	void ReleaseSub();

	void Transform();

	//自分を消す
	void KillMe();

	void Collision( IGameObject* targetobject );
	void AddCollider(Collider * collider);
	virtual void OnCollision(IGameObject* ptarget) {};

	//親オブジェクトを取得
	//戻値：親オブジェクトのアドレス
	IGameObject* GetParent();

	D3DXVECTOR3 Getposition();
	D3DXVECTOR3 Getrotate();
	D3DXVECTOR3 Getscale();

	//指定のオブジェクトを子として入れる
	void PushBackChild(IGameObject* pObj);

	//positionのセッター
	void SetPosition(D3DXVECTOR3 position)
	{
		position_ = position;
	}

	//Colliderのセッター
	void SetCollider(D3DXVECTOR3& center, float radius);

	//Updataを実行していいか
	bool IGameObject::IsEntered() {
		return (state_.entered != 0);
	}

	//子リストを送る
	std::list<IGameObject*>* IGameObject::GetChildList() {
		return &pChildlist_;
	}

	//初期化済みかどうか
	bool IGameObject::IsInitialized() {
		return (state_.entered != 0);
	}

	//初期化済みにする
	void IGameObject::SetInitialized(){
		state_.initialized = 1;
	}

	//削除するかどうか
	bool IGameObject::IsDead() {
		return (state_.dead != 0);
	}

	//Draw実行していいか
	bool IGameObject::IsVisibled() {
		return (state_.visible != 0);
	}

	//名前でオブジェクトを検索（対象は自分の子供以下）
	//引数：name	検索する名前
	//戻値：見つけたオブジェクトのアドレス（見つからなければnullptr）
	IGameObject* FindChildObject(const std::string& name);

	//名前でオブジェクトを検索（対象は全体）
	//引数：検索する名前
	//戻値：見つけたオブジェクトのアドレス
	IGameObject* FindObject(const std::string& name) { return /*IGameObjectManager::*/FindChildObject(name); }

	const std::string & GetObjectName(void) const;

};

//テンプレートを作成
template <class T>

//GameObjectを作成(自動的にもらったクラスに設定される)
T* CreateGameObject(IGameObject* parent)
{
	T* p = new T(parent);
	parent->PushBackChild(p);
	p->Initialize();
	return p;
}